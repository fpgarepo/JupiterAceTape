/*  tzx2tap.c

	Convert ZX Spectrum TZX file with data for JupiterACE to JupiterACE TAP file.

	2020-05-16 Hagen Patzke - First version, only works with block type 0x10 (Standard Speed Data Block)
*/

#pragma warning(disable : 4996)


#include <string.h>  // strncmp
#include <stdio.h>
#include <stdlib.h>

typedef unsigned char  u08;
typedef unsigned short u16;
typedef unsigned long  u32;


FILE *fd_tzx;
FILE *fd_tap;

void main(int argc, char * argv[]) {
	int rc;
	u08 tzx_header[10];
	u08 tzx_blkid;
	u08 tzx_blk10[4];
	u16 tzx_len_pause;
	u16 tzx_len_data;
	u08 tap_type;
	u08 tap_len[2];
	u08 buf[65536];


	char *tzx_name = "in.tzx";
	char *tap_name = "out.tap";

	if (argc < 2 || argc > 3)
		exit(printf("tzx2tap <tzx file> [<tap file>]\n"));

	tzx_name = argv[1];
	if (argc == 3) 
		tap_name = argv[2];

	fd_tzx = fopen(tzx_name, "rb");
	if (fd_tzx < 0)
		exit(printf("Couldn't open TZX file....\n"));

	fd_tap = fopen(tap_name, "wb");
	if (fd_tzx < 0)
		exit(printf("Couldn't open TAP file....\n"));

	rc = fread(tzx_header, 10, 1, fd_tzx);
	if (rc != 1)
		exit(printf("Can't read TZX header...\n"));

	if(strncmp(tzx_header, "ZXTape!", 7) != 0 || tzx_header[7] != 0x1A)
		exit(printf("Invalid TZX header. Abort.\n"));

	printf("Reading TZX file version %d.%d\n", tzx_header[8], tzx_header[9]);

	while (1) {
		rc = fread(&tzx_blkid, 1, 1, fd_tzx);
		if (rc != 1) {
			printf("Can't read TZX block ID... finished.\n");
			break;
		}
		if (tzx_blkid != 0x10)
			exit(printf("Can't handle TZX block with ID 0x%02x\n", tzx_blkid));
		rc = fread(tzx_blk10, 4, 1, fd_tzx);
		if (rc != 1)
			exit(printf("Can't read TZX block type 0x10 header\n"));
		tzx_len_pause = tzx_blk10[0] | tzx_blk10[1] << 8;
		tzx_len_data  = tzx_blk10[2] | tzx_blk10[3] << 8;
		// Handle one TAP block
		rc = fread(&tap_type, 1, 1, fd_tzx);
		if (rc != 1)
			exit(printf("Can't read TZX TAP block type\n"));
		printf(" - 0x10 block len: 0x%04x, TAP block type: 0x%02x, , pause: %dms\n", 
			tzx_len_data, tap_type, tzx_len_pause);
		tzx_len_data--;
		tap_len[0] = tzx_len_data & 0xFF;
		tap_len[1] = tzx_len_data >> 8;
		rc = fwrite(tap_len, 2, 1, fd_tap);
		if (rc != 1)
			exit(printf("Can't write TAP block size...\n"));
		rc = fread(buf, tzx_len_data, 1, fd_tzx);
		if (rc != 1)
			exit(printf("Can't read TZX TAP block...\n"));
		rc = fwrite(buf, tzx_len_data, 1, fd_tap);
		if (rc != 1)
			exit(printf("Can't write TAP block...\n"));
	}
	fclose(fd_tap);
	fclose(fd_tzx);

	printf("OK.\n");
}
