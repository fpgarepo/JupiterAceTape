/*  wav2tap.c

	Convert JupiterACE tape output from a WAV file to TAP format.

	2020-05-06 Hagen Patzke:
	 - support 16 bit signed PCM samples
	 - skip unwanted WAV Chunks (made by e.g. Zoom H1)
	 - support channel selection (e.g. stereo has 0 or 1)

    Derived from TAPH.C.
	Based on the theory behind TAPH.PAS by TechnoMancer.
*/

#pragma warning(disable : 4996)


#include <stdio.h>
#include <stdlib.h>

typedef unsigned char  u08;
typedef unsigned short u16;
typedef unsigned long  u32;

int threshold_base_fq = 22000;
int threshold_onebit = 8;
int threshold_lo_header = 11;
int threshold_hi_header = 17;


struct _ACE_HEADER {
	u08 Type; // 0 = dict, 32 = binary (or any other)
	u08 Name[10]; // filename, padded with spaces
	u16 Size;
	u16 StartAddress; // 15441 for dict
	u16 CurrentWord; // unused for binary (space)
	u16 SysVar_CURRENT; // unused for binary (space)
	u16 SysVar_CONTEXT; // unused for binary (space)
	u16 SysVar_VOCLNK; // unused for binary (space)
	u16 SysVar_STKBOT; // unused for binary (space)
	u08 xor_checksum;
} ACE_Header;

u08 ACE_Header_Bin[25];

struct _WAV_HEADER {
	u32 ChunkID;	// "RIFF" (0x52494646 big-endian form)
	u32 ChunkSize;	// rest of file size
	u32 Format; // "WAVE" (0x57415645 big-endian form)
} WAV_Header;

struct _WAV_TAGLEN {
	u32 ChunkID;
	u32 ChunkSize;
} WAV_TagLen;

struct _FMT_SUBHEADER {
	// ChunkID   is "fmt " (0x666d7420 big-endian form)
	// ChunkSize should be 16 for PCM
	u16 AudioFormat; // 1 = linear quantization (others are compressed)
	u16 NumChannels; // 1 = Mono, 2 = Stereo
	u32 SampleRate; // 22000, 44100, etc.
	u32 ByteRate; // == SampleRate * NumChannels * BitsPerSample/8
	u16 BlockAlign; // == NumChannels * BitsPerSample/8
	u16 BitsPerSample; //  8 bits = 8, 16 bits = 16, etc.
	// if not PCM, then here is space for ExtraParamSize end ExtraParams
} FMT_Chunk;

// _DATA_HEADER is empty
	// ChunkID   is "data" (0x64617461 big-endian form)
	// ChunkSize is size of data == NumSamples * NumChannels * BitsPerSample/8
	// follows: Data (little-endian)

FILE *fd_wav;
FILE *fd_tap;

static int wav_use_channel = 0; /* for WAV file with multiple channels */

char *ChunkID2String(u32 chunkid) {
	static char s[] = "0000";
	for (int i = 0; i < 4; i++) {
		s[i] = (char)(chunkid & 0xFF);
		chunkid >>= 8;
	}
	return s;
}

int WaveOpen(char * filename) {
	fd_wav = fopen(filename, "rb");
	fread(&WAV_Header, sizeof(WAV_Header), 1, fd_wav);
	if (WAV_Header.ChunkID != 0x46464952l || WAV_Header.Format != 0x45564157l) { // "RIFF" and "WAVE"
		printf("ERROR: '%s' is not a WAVE file\n", filename);
		return 1;
	}

	printf("Reading WAVE file '%s'...\n", filename);

	int have_fmt = 0;
	while (1) {
		fread(&WAV_TagLen, sizeof(WAV_TagLen), 1, fd_wav);
		printf("  Chunk '%s', size: %lu bytes\n", ChunkID2String(WAV_TagLen.ChunkID), WAV_TagLen.ChunkSize);
		if (WAV_TagLen.ChunkID == 0x20746d66l) { // "fmt "
			have_fmt++;
			fread(&FMT_Chunk, sizeof(FMT_Chunk), 1, fd_wav);
			printf("    AudioFormat   : %d\n    NumChannels   : %d\n    SampleRate    : %lu\n    BitsPerSample : %d\n    BlockAlign    : %lu\n",
				FMT_Chunk.AudioFormat, FMT_Chunk.NumChannels, FMT_Chunk.SampleRate, FMT_Chunk.BitsPerSample, FMT_Chunk.BlockAlign);
			int skip = WAV_TagLen.ChunkSize - sizeof(FMT_Chunk);
			if (skip) {
				fseek(fd_wav, skip, SEEK_CUR);
			}
			if (FMT_Chunk.AudioFormat != 1) {
				printf("ERROR: '%s' has an unrecognized audio format (not 1)\n", filename);
				return 1;
			}
			if (FMT_Chunk.BitsPerSample != 8 && FMT_Chunk.BitsPerSample != 16) {
				printf("ERROR: '%s' has sample size %d we cannot process (only 8 and 16 bit)\n", filename, FMT_Chunk.BitsPerSample);
				return 1;
			}
			if (threshold_base_fq != FMT_Chunk.SampleRate) {
				float ratio = (float)FMT_Chunk.SampleRate / threshold_base_fq;
				threshold_onebit = (int)(ratio * threshold_onebit);
				threshold_lo_header = (int)(ratio * threshold_lo_header);
				threshold_hi_header = (int)(ratio * threshold_hi_header);
				printf("    Sample threshold correction from %d kHz:\n", threshold_base_fq);
				printf("      threshold_onebit    : %d\n", threshold_onebit);
				printf("      threshold_lo_header : %d\n", threshold_lo_header);
				printf("      threshold_hi_header : %d\n", threshold_hi_header);
				threshold_base_fq = FMT_Chunk.SampleRate;
				printf("      new base_frequency  : %d kHz\n", threshold_base_fq);
			}
		}
		else if (WAV_TagLen.ChunkID == 0x61746164l) {
			if (have_fmt)
				return 0;
			// if we had no format block, then this is an error
			printf("ERROR: '%s' does not have a 'fmt ' chunk\n", filename);
			return 1;
		} else {
			printf("  ...skipped.\n");
			fseek(fd_wav, WAV_TagLen.ChunkSize, SEEK_CUR);
		}
	}

	return 0; // notreached
}

void TAPOpen(char * filename) {
	fd_tap = fopen(filename, "wb");
}

int GetNextSample(void) {
	int s = -1;
	int rs = -1;
	for (int ch = 0; ch < FMT_Chunk.NumChannels; ch++) {
		switch (FMT_Chunk.BitsPerSample) {
			case 8: {
				u08 buf;
				if (fread(&buf, 1, 1, fd_wav) == 1) { // 8bit sample, unsigned
					rs = buf;
					//printf("%d(%d->%d)", ch, buf, rs);
				}
			} break;
			case 16: {
				short int buf;
				if (fread(&buf, 2, 1, fd_wav) == 1) { // 16bit sample, signed
					rs = ((unsigned short)(buf + (1 << 15))) >> 8;
					//printf("%d(%d->%d)", ch, buf, rs);
				}
			} break;
		}
		if (ch == wav_use_channel) {
			s = rs;
		}
	}
	return s;
}


int GetEdgeSize(void) {
	static int flip = 0;	/* 0 = last went up, 1 = last down */
	int n1, n2, n3;
	int count = 0;
	n1 = n2 = n3 = ((flip == 0) ? 255 : 0); /* Initial values */
	while (1) {
		n3 = GetNextSample(); // expects value from 0..255
		if (n3 < 0) break; // read error
		count++;
		/* When in order and zero-x */
		if ((n1 < n2 && n2 < n3 && n1 < 128 && n3 > 128) ||
			(n1 > n2 && n2 > n3 && n1 > 128 && n3 < 128)) {
			flip = (n1 < n2) ? 0 : 1; /* Which way are we going ? */
			return count; /* return distance */
		}
		n1 = n2;
		n2 = n3; /* Otherwise rotate next in */
	}
	return 0; // error
}


u08 ReadByte() {
	int e1, e2, b, n;
	b = 0;
	for (n = 0; n < 8; n++) {  /* Read in a bit at a time */
		b <<= 1;
		e1 = GetEdgeSize();
		e2 = GetEdgeSize(); /* One complete wave per bit */
		/*		printf("%d:%d ",e1,e2); */
		if (e1 > threshold_onebit) b++; /* If at 22kHz, leading edge > 8 its a '1' */
	}
	return (u08)b;
}


void ReadBlock(u08 * buf, int len) {
	unsigned long count_leader = 0u;
	int i, b, cs;

	len++; // add the checksum (XOR) byte to the length

	do { // Skip lead-in
		i = GetEdgeSize();
		count_leader++;
	} while (i >= threshold_lo_header && i < threshold_hi_header);

	// Print statistics, skip second Sync mark
	printf("  LEAD count: %lu, SYNC %d %d\n", count_leader, i, GetEdgeSize());

	// Skip Header/Data indicator 0x00 / 0xFF (print it for info)
	{
		u08 type = ReadByte();
		printf("  Subblock type %d (%s)\n", type, (type == 0) ? "Header" : (type == 255) ? "Data" : "Unknown");
	}

	fputc(len & 0xFF, fd_tap); /* Write header (length) to output file */
	fputc(len >> 8, fd_tap);

	cs = 0; // init checksum
	for (i = 0; i < len; i++) /* Copy bytes */ {
		b = ReadByte();
		cs ^= b; // update checksum
		/*	printf("%02x %c ",b,b & 0x7F); */
		fputc(b, fd_tap); /* Write to .TAP file */
		if (buf != NULL) { buf[i] = b; } /* Store in header if applicable */
	}
	// last byte read from block is XOR checksum byte
	printf("  Read block %d bytes (len + checksum %d - %s).\n", len, b, (cs==0) ? "OK" : "FAIL");
	// Skip end mark
	printf("  END_MARK %d %d\n", GetEdgeSize(), GetEdgeSize());
}

void main(int argc, char * argv[]) {
	int rc, i;

	if (argc < 2 || argc > 3) 
		exit(printf("wav2tap <wave file> [channel]\n"));

	rc = WaveOpen(argv[1]);
	if (rc || fd_wav == NULL) 
		exit(printf("Couldn't open WAV file....\n"));

	if (argc == 3) 
		wav_use_channel = atoi(argv[2]);

	TAPOpen("out.tap");
	if (fd_tap == NULL) 
		exit(printf("Couldn't open TAP file....\n"));

	for (i = 0; i < 14; i++) 
		GetEdgeSize(); /* Skip any junk */


	printf("READ TAPE DATA HEADER\n");
	ReadBlock(ACE_Header_Bin, sizeof(ACE_Header_Bin));

	printf("  Header Data\n");
	int btype = ACE_Header_Bin[0];
	int bsize = *((u16*)(&ACE_Header_Bin[11]));
	printf("    Block Type : 0x%02x (%s)\n", btype, (btype == 0) ? "dict" : "binary");
	printf("    Block Name : ");
	for (i = 1; i < 11; i++) printf("%c", ACE_Header_Bin[i] & 0x7F);
	printf("\n");
	printf("    Block Size : %d\n", bsize);

	printf("READ TAPE DATA BODY\n");
	ReadBlock(NULL, bsize); /* Read body */

	fclose(fd_wav);
	fclose(fd_tap);
}
