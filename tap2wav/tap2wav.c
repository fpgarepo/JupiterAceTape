/*  tap2wav.c

	Convert JupiterACE TAP file to a WAV file.

	2020-05-06 Hagen Patzke
*/

#pragma warning(disable : 4996)


#include <stdio.h>
#include <stdlib.h>

typedef unsigned char  u08;
typedef unsigned short u16;
typedef unsigned long  u32;

typedef enum {
	PILOT_H = 2011, PILOT_L = 2011,
	SYNC_H = 601, SYNC_L = 791,
	ENDM_H = 903, ENDM_L = 4187,
	BIT0_H = 795, BIT0_L = 801,
	BIT1_H = 1585, BIT1_L = 1591,
	COUNT_PILOT_HEADER = 4096,
	COUNT_PILOT_DATA = 512,
	BTYPE_HEADER = 0x00,
	BTYPE_DATA = 0xFF,
} ACE_TAPE_CONTANTS;

static const u32 ace_base_freq_hz = 3250000;
static const u32 wav_base_freq_hz = 22500; // half CD quality
static float ratio_ace2wav = 144.4444f; // ace_base_freq_hz / wav_base_freq_hz;

static const u08 tapeout_hi = 0xE0;
static const u08 tapeout_lo = 0x20;
static const u08 silence_hi = 0x80;
static const u08 silence_lo = 0x7F;

u32  wav_data_size = 0;

//struct _ACE_HEADER {
//	u08 Type; // 0 = dict, 32 = binary (or any other)
//	u08 Name[10]; // filename, padded with spaces
//	u16 Size;
//	u16 StartAddress; // 15441 for dict
//	u16 CurrentWord; // unused for binary (space)
//	u16 SysVar_CURRENT; // unused for binary (space)
//	u16 SysVar_CONTEXT; // unused for binary (space)
//	u16 SysVar_VOCLNK; // unused for binary (space)
//	u16 SysVar_STKBOT; // unused for binary (space)
//	u08 xor_checksum;
//} ACE_Header;

u08 ACE_Header_Bin[26];

struct _WAV_HEADER {
	u32 idRiff;	// "RIFF" (0x52494646 big-endian form)
	u32 szRiff;	// rest of file size
	u32 Format; // Format "WAVE" (0x57415645 big-endian form)
	u32 idFmt;	// "fmt " (0x666d7420 big-endian form)
	u32 szFmt;  // normally 16 for PCM data
	u16 AudioFormat; // 1 = linear quantization (others are compressed)
	u16 NumChannels; // 1 = Mono, 2 = Stereo
	u32 SampleRate; // 22500, 44100, etc.
	u32 ByteRate; // == SampleRate * NumChannels * BitsPerSample/8
	u16 BlockAlign; // == NumChannels * BitsPerSample/8
	u16 BitsPerSample; //  8 bits = 8, 16 bits = 16, etc.
	u32 idData;	// "data" (0x64617461 big-endian form)
	u32 szData;	// == NumSamples * NumChannels * BitsPerSample/8
} WAV_Header;

FILE *fd_tap;
FILE *fd_wav;

u32 String2ChunkID(const unsigned char * s) {
	u32 v = 0;
	for (int i = 0; i < 4; i++) {
		v = v | (s[i] << (i * 8));
	}
	return v;
}

int WaveOpen(char * filename, u32 datasize) {
	wav_data_size = datasize;
	
	fd_wav = fopen(filename, "wb");
	WAV_Header.idRiff = String2ChunkID("RIFF");
	WAV_Header.szRiff = datasize + 36;
	WAV_Header.Format = String2ChunkID("WAVE");
	WAV_Header.idFmt  = String2ChunkID("fmt ");
	WAV_Header.szFmt  = 16;
	WAV_Header.AudioFormat = 1;
	WAV_Header.NumChannels = 1; // mono is enough
	WAV_Header.SampleRate = wav_base_freq_hz;
	WAV_Header.ByteRate = wav_base_freq_hz;
	WAV_Header.BlockAlign = 1;
	WAV_Header.BitsPerSample = 8; // unsigned 8-bit
	WAV_Header.idData = String2ChunkID("data");
	WAV_Header.szData = datasize;

	if (fwrite(&WAV_Header, sizeof(WAV_Header), 1, fd_wav) == 0) {
		printf("ERROR: Cannot open '%s' for writing.\n", filename);
		return 1;
	}

	return 0;
}

int WaveClose(char * filename, u32 datasize) {
	int rc = 0;

	u32 buf = datasize + 36;
	fseek(fd_wav, 4, SEEK_SET);
	if (fwrite(&buf, 4, 1, fd_wav) == 0) {
		printf("ERROR: Cannot write 'RIFF' size to '%s'.\n", filename);
		rc++;
	}
	if (!rc) {
		buf = datasize;
		fseek(fd_wav, 40, SEEK_SET);
		if (fwrite(&buf, 4, 1, fd_wav) == 0) {
			printf("ERROR: Cannot write 'data' size to '%s'.\n", filename);
			rc++;
		}
	}

	fclose(fd_wav);
	return rc;
}


int TAPOpen(char * filename) {
	fd_tap = fopen(filename, "rb");
	if (fd_tap == NULL) {
		printf("ERROR: Cannot open '%s' for reading.\n", filename);
		return 1;
	}
	return 0;
}

void WriteSample(int p, u08 level) {
	static float ace_time_pos = 0.0f;

	ace_time_pos += p;
	while (ace_time_pos > ratio_ace2wav) {
		fwrite(&level, 1, 1, fd_wav);
		wav_data_size++;
		ace_time_pos -= ratio_ace2wav;
	}
}

void WriteSilence() {
	// We want to write several silence samples, the size of the pilot signal seems OK.
	WriteSample(PILOT_H, silence_hi);
	WriteSample(PILOT_L, silence_lo);
}

void WriteEdge(int p_hi, int p_lo) {
	WriteSample(p_hi, tapeout_hi);
	WriteSample(p_lo, tapeout_lo);
}

void WriteByte(u08 b) {
	for (int k=0; k<8; k++){
		if (b & 0x80)
			WriteEdge(BIT1_H, BIT1_L);
		else
			WriteEdge(BIT0_H, BIT0_L);

		b <<= 1;
	}
}

void MakeName(char * buf, u08 * ace, int blockno) {
	int k, e = 0;
	char c, wbuf[11];
	for (k = 0; k < 10; k++) {
		c = (char) (ace[k] & 0x7F);
		if (c < 32) {
			e++;
			c = '0';
		}
		wbuf[k] = (c==32) ? '\0' : c;
		if (c == 32) break;
	}
	wbuf[10] = 0;
	if (e > 4) {
		sprintf(buf, "out%03d.wav", blockno);
	} else {
		if (blockno == 0) {
			sprintf(buf, "%s.wav", wbuf);
		} else {
			sprintf(buf, "%s.%d.wav", wbuf, blockno);
		}
	}
}

// Convert one block from TAP to WAV.
// return 0 if end-of-file or error
// return 1 if conversion of one block was successful
int ConvertBlock(FILE * fd_tap) {
	static int blockno = 0;
	int carry_on = 1;
	int i;
	u16 size = 0;

	if (fread(&size, 2, 1, fd_tap) == 0) {
		printf("End of TAP file.\n");
		return 0;
	}

	if (size != sizeof(ACE_Header_Bin)) {
		printf("Wrong header size: %d bytes (should be 26 bytes).\n", size);
		return 0;
	}

	if (fread(&ACE_Header_Bin, sizeof(ACE_Header_Bin), 1, fd_tap) == 0) {
		printf("Error: premature end of TAP file (in header)");
		return 0;
	}

	// check header name
	char wav_name[32];
	MakeName(wav_name, &ACE_Header_Bin[1], blockno++);

	printf("WRITE TAPE DATA HEADER\n");
	printf("  Header Data\n");
	int btype = ACE_Header_Bin[0];
	int bsize = *((u16*)(&ACE_Header_Bin[11]));
	printf("    Block Type : 0x%02x (%s)\n", btype, (btype == 0) ? "dict" : "binary");
	printf("    Block Name : ");
	for (i = 1; i < 11; i++) printf("%c", ACE_Header_Bin[i] & 0x7F);
	printf("\n");
	printf("    Block Size : %d\n", bsize);

	if (WaveOpen(wav_name, 0)) {
		printf("ERROR: Couldn't open WAV file....\n");
		return 0;
	}

	WriteSilence();

	// Write the header block
	for (i = 0; i < COUNT_PILOT_HEADER; i++)
		WriteEdge(PILOT_H, PILOT_L);
	WriteEdge(SYNC_H, SYNC_L);
	WriteByte(BTYPE_HEADER); // the header/data indicator is not part of the TAP file
	for (i = 0; i < size; i++)
		WriteByte(ACE_Header_Bin[i]);
	WriteEdge(ENDM_H, ENDM_L);

	// Get the data block size
	if (fread(&size, 2, 1, fd_tap) == 0) {
		printf("Error: premature end of TAP file");
		carry_on = 0;
	}

	if (carry_on) {
		printf("READ TAPE DATA BODY\n");
		printf("  Block size: %d\n", size);

		// Write the data block
		for (i = 0; i < COUNT_PILOT_DATA; i++)
			WriteEdge(PILOT_H, PILOT_L);
		WriteEdge(SYNC_H, SYNC_L);
		WriteByte(BTYPE_DATA);
		for (i = 0; i < size; i++) {
			u08 buf;
			fread(&buf, 1, 1, fd_tap);
			WriteByte(buf);
		}
		WriteEdge(ENDM_H, ENDM_L);
	}
	
	WriteSilence();

	if (WaveClose(wav_name, wav_data_size)) {
		printf("Error closing WAV file.\n");
		carry_on = 0;
	}

	return carry_on;
}

void main(int argc, char * argv[]) {
	int rc;
	char * wav_name = "out.wav";

	ratio_ace2wav = ((float)ace_base_freq_hz) / wav_base_freq_hz;

	if (argc != 2)
		exit(printf("tap2wav <tap file>\n"));

	rc = TAPOpen(argv[1]);
	if (rc)
		exit(printf("Couldn't open TAP file....\n"));
	   	 

	while (ConvertBlock(fd_tap)) {
		// successful conversion, continue
	}


	fclose(fd_tap);

	printf("OK.\n");
}
